open Format

type 'a register =  string
type word = [ `word ]

type double = [ `double ]

let v0 : word register = "$v0"
let v1 : word register = "$v1"
let a0 : word register = "$a0"
let a1 : word register = "$a1"
let a2 : word register = "$a2"
let a3 : word register = "$a3"
let t0 : word register = "$t0"
let t1 : word register = "$t1"
let t2 : word register = "$t2"
let t3 : word register = "$t3"
let s0 : word register = "$s0"
let s1 : word register = "$s1"
let ra : word register = "$ra"
let sp : word register = "$sp"
let fp : word register = "$fp"
let gp : word register = "$gp"
let zero : word register = "$zero"


let f0 : double register = "$f0"
let f2 : double register = "$f2"
let f4 : double register = "$f4"
let f6 : double register = "$f6"
let f8 : double register = "$f8"
let f10 : double register = "$f10"
let f12 : double register = "$f12"


type label = string

type 'a address = formatter -> 'a -> unit


let alab : label address = fun fmt  (s : label) -> fprintf fmt "%s" s
let areg : (int * 'a register) address = fun fmt (x, y) -> fprintf fmt "%i(%s)" x y
type 'a operand = formatter -> 'a -> unit
let oreg : 'a register operand = fun fmt (r : 'a register) -> fprintf fmt "%s" r
let oi : int operand = fun fmt i -> fprintf fmt "%i" i
let oi32 : int32 operand = fun fmt i -> fprintf fmt "%li" i

type 'a asm =
  | Nop
  | S of string
  | Cat of 'a asm * 'a asm

type text = [`text ] asm
type data = [`data ] asm


let ins x =
  let buf = Buffer.create 17 in
  let fmt = formatter_of_buffer buf in
  fprintf fmt "\t";
  kfprintf (fun _ ->
    fprintf fmt "@\n";
    pp_print_flush fmt () ;
    let s = Buffer.contents buf in
    Buffer.clear buf; S s
  ) fmt x


let pr_list fmt pr l =
  match l with
    [] -> ()
  | [ i ] -> pr fmt i
  | i :: ll -> pr fmt i;
      List.iter (fun i -> fprintf fmt ", %a" pr i) ll

let pr_ilist fmt l =
  pr_list fmt (fun fmt i -> fprintf fmt "%i" i) l

let pr_alist fmt l =
  pr_list fmt (fun fmt (a : label) -> fprintf fmt "%s" a) l

let pr_flist fmt l =
  pr_list fmt (fun fmt d -> fprintf fmt "%f" d) l

let abs (r1 : word register) (r2 : word register) = ins "abs %s, %s" r1 r2

let add (r1 : word register) (r2 : word register) (o : 'a operand) = ins "add %s, %s, %a" r1 r2 o
let clz (r1 : word register) (r2 : word register) = ins "clz %s, %s" r1 r2
let and_ (r1 : word register) (r2 : word register) (r3 : word register) = ins "and %s, %s, %s" r1 r2 r3
let div (r1 : word register) (r2 : word register) (o : 'a operand) = ins "div %s, %s, %a" r1 r2 o
let mul (r1 : word register) (r2 : word register) (o : 'a operand) = ins "mul %s, %s, %a" r1 r2 o
let or_ (r1 : word register) (r2 : word register) (r3 : word register) = ins "or %s, %s, %s" r1 r2 r3
let not_ (r1 : word register) (r2 : word register) = ins "not %s, %s" r1 r2
let rem (r1 : word register) (r2 : word register) (o : 'a operand) = ins "rem %s, %s, %a" r1 r2 o
let neg (r1 : word register) (r2 : word register) = ins "neg %s, %s" r1 r2
let sub (r1 : word register) (r2 : word register) (o : 'a operand) = ins "sub %s, %s, %a" r1 r2 o
let li (r : word register) i = ins "li %s, %i" r i
let li32 (r : word register) i = ins "li %s, %li" r i
let seq (r1 : word register) (r2 : word register) (r3 : word register) = ins "seq %s, %s, %s" r1 r2 r3
let sge (r1 : word register) (r2 : word register) (r3 : word register) = ins "sge %s, %s, %s" r1 r2 r3
let sgt (r1 : word register) (r2 : word register) (r3 : word register) = ins "sgt %s, %s, %s" r1 r2 r3
let sle (r1 : word register) (r2 : word register) (r3 : word register) = ins "sle %s, %s, %s" r1 r2 r3
let slt (r1 : word register) (r2 : word register) (r3 : word register) = ins "slt %s, %s, %s" r1 r2 r3
let sne (r1 : word register) (r2 : word register) (r3 : word register) = ins "sne %s, %s, %s" r1 r2 r3
let b (z : label) = ins "b %s" z
let beq (r1 : word register) (r2 : word register) (l : label) = ins "beq %s, %s, %s" r1 r2 l
let bne (r1 : word register) (r2 : word register) (l : label) = ins "bne %s, %s, %s" r1 r2 l
let bge (r1 : word register) (r2 : word register) (l : label) = ins "bge %s, %s, %s" r1 r2 l
let bgt (r1 : word register) (r2 : word register) (l : label) = ins "bgt %s, %s, %s" r1 r2 l
let ble (r1 : word register) (r2 : word register) (l : label) = ins "ble %s, %s, %s" r1 r2 l
let blt (r1 : word register) (r2 : word register) (l : label) = ins "blt %s, %s, %s" r1 r2 l

let beqz (r : word register) (l : label) = ins "beqz %s, %s" r l
let bnez (r : word register) (l : label) = ins "bnez %s, %s" r l
let bgez (r : word register) (l : label) = ins "bgez %s, %s" r l
let bgtz (r : word register) (l : label) = ins "bgtz %s, %s" r l
let blez (r : word register) (l : label) = ins "blez %s, %s" r l
let bltz (r : word register) (l : label) = ins "bltz %s, %s" r l

let jr (r : word register) = ins "jr %s" r
let jal (z : label) = ins "jal %s" z
let jalr (z : word register) = ins "jalr %s" z
let la (r : word register) (l : 'a address) = ins "la %s, %a" r l
let lb (r : word register) (l : 'a address) = ins "lb %s, %a" r l
let lbu (r : word register) (l : 'a address) = ins "lbu %s, %a" r l
let lw (r : word register) (l : 'a address) = ins "lw %s, %a" r l
let sb (r : word register) (l : 'a address) = ins "sb %s, %a" r l
let sw (r : word register) (l : 'a address) = ins "sw %s, %a" r l
let move (r1 : word register) (r2 : word register) = ins "move %s, %s" r1 r2

let nop = Nop

(* floating point *)
let abs_d (r1 : double register) (r2 : double register) = ins "abs.d %s %s" r1 r2
let add_d (r1 : double register) (r2 : double register) (r3 : double register) = ins "add.d %s %s %s" r1 r2 r3
let c_eq_d (r1 : double register) (r2 : double register) = ins "c.eq.d %s %s" r1 r2
let c_le_d (r1 : double register) (r2 : double register) = ins "c.le.d %s %s" r1 r2
let c_lt_d (r1 : double register) (r2 : double register) = ins "c.lt.d %s %s" r1 r2
let cvt_d_w (r1 : double register) (r2 : double register) = ins "cvt.d.w %s %s" r1 r2
let cvt_w_d  (r1 : double register) (r2 : double register) = ins "cvt.w.d %s %s" r1 r2
let div_d  (r1 : double register) (r2 : double register) (r3 : double register) = ins "div.d %s %s %s" r1 r2 r3
let l_d (r1 : double register) (l : 'a address) = ins "l.d %s %a" r1 l
let mov_d (r1 : double register) (r2 : double register) = ins "mov.d %s %s" r1 r2
let mfc1 (r1 : word register) (f1 : double register) = ins "mfc1 %s %s" r1 f1
let mtc1 (r1 : word register) (f1 : double register) = ins "mtc1 %s %s" r1 f1

let mul_d (r1 : double register) (r2 : double register) (r3 : double register) = ins "mul.d %s %s %s" r1 r2 r3
let neg_d (r1 : double register) (r2 : double register) = ins "neg.d %s %s" r1 r2
let s_d (r1 : double register) (l : 'a address) = ins "s.d %s %a" r1 l
let sub_d (r1 : double register) (r2 : double register) (r3 : double register) = ins "sub.d %s %s %s" r1 r2 r3

let bc1f (l : label) = ins "bc1f %s" l
let bc1t (l : label) = ins "bc1t %s" l
let lwc1 (f : double register) (l : 'a address) = ins "lwc1 %s, %a" f l
let swc1 (f : double register) (l : 'a address) = ins "swc1 %s, %a" f l

let label (s : label) = S ( s ^ ":\n")
let syscall = S "\tsyscall\n"
let comment s = S ("#" ^ s ^ "\n")
let align i = ins ".align %i" i
let asciiz s = ins ".asciiz %S" s
let word l = ins ".word %a" pr_ilist l
let address l = ins ".word %a" pr_alist l
let double l = ins ".double %a" pr_flist l

let (@@) x y = Cat (x, y)
let (++) x y = x @@ y


let set_stack_alignment, get_stack_alignment =
  let stack_alignment = ref 8 in
  let s = fun n ->
  if n land (n-1) == 0 then
    stack_alignment := n
  else
    failwith "Invalid stack alignment (must be a power of two)"
  in
  let g = fun () -> !stack_alignment
  in
  s, g

(* gruik *)

let is_double_r r =
  r <> "$fp" && r.[1] = 'f'

let double_r : 'a register -> double register = fun r -> if is_double_r r then r else assert false

let word_r : 'a register -> word register = fun r -> if not(is_double_r r) then r else assert false

let push r =
  sub sp sp oi (get_stack_alignment())
  @@ if is_double_r r
    then s_d (double_r r) areg (0,sp)
    else sw (word_r r) areg (0, sp)

let peek r =
  if is_double_r r
  then l_d (double_r r) areg (0,sp)
  else lw (word_r r) areg (0,sp)

let pop r =
  peek r
    @@ add sp sp oi (get_stack_alignment())


type program = { text : [ `text ] asm;
                 data : [ `data ] asm; }

let rec pr_asm fmt a =
  match a with
  | Nop -> ()
  | S s -> fprintf fmt "%s" s
  | Cat (a1, a2) ->
      let () = pr_asm fmt a1 in
      pr_asm fmt a2

let print_program fmt p =
  fprintf fmt ".text\n";
  pr_asm fmt p.text;
  fprintf fmt ".data\n";
  pr_asm fmt p.data
