val file : Ast.loc Ast.file -> Ast.c_type Ast.file
val is_lvalue : 'info Ast.expr -> bool
val struct_env : (string, Ast.var_decl list) Hashtbl.t
val type_to_string : Ast.c_type -> string
exception Error of Ast.loc * string
