open Format
open Mips
open Ast


let size_of_struct id vl = 0

let size_of_type t =
	match t with 
	| Tnull -> assert false
	| Tvoid -> assert false
	| Tint | Tpointer t  -> 4
	| Tchar -> 1
	| Tstruct id  -> size_of_struct id vl
	| Tdouble -> 8

let push_var id h =
	let size = size_of_type id.loc in
		Hashtbl.add h id.node fp


let compile_decl decl (text, data) =
	match decl with
	| Dvars vl -> 
	| Dstruc (id, vl) ->
	| Dfun  (t_ret, id, vl, bl) -> 
		let var_pos = Hashtbl.create 20 in
		let text2 = List.fold_left (fun acc (t, id) -> acc @@ push_var id t h) text vl in 
		let text3 = text 
		@@ label id


let compile_file f =
	let (text, data) = List.fold_left (fun decl_acc decl -> 
										compile_decl decl decl_acc
										 ) ("","") f
	let text = "" in
	let data = "" in
	{
		text = text;
		data = data;

	}